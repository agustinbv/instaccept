export default {
  // en segundos
  interval: {
    check: 60, // fijo
    requests: 1 // random entre 0 y este valor
  },
  // no auto-aceptar amigos en comun
  skipSuggestions: true
}