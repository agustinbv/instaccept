require('console-stamp')(console);

import { 
  IgApiClient, 
  IgCheckpointError, 
  IgLoginTwoFactorRequiredError,
  IgResponseError
} from 'instagram-private-api'

import interval from 'interval-promise'
import inquirer from 'inquirer'
import Bluebird from 'bluebird'
import { delay } from 'bluebird'
import config from './config'

const ig = new IgApiClient()

async function login () {
  const { username, password } = await inquirer.prompt([
    { 
      type: 'input', 
      name: 'username', 
      message: 'Usuario' 
    },
    { 
      type: 'password', 
      name: 'password', 
      message: 'Password',
      mask: true
    }
  ])
  // Login
  await Bluebird.try(async () => {
    ig.state.generateDevice(username);
    await ig.simulate.preLoginFlow(); // opcional, pero recomendado segun la lib
    const account = await ig.account.login(username, password);
    process.nextTick(async () => await ig.simulate.postLoginFlow()); // opcional, pero recomendado segun la lib
    console.log('login OK')
  })
  // Two-factor
  .catch(IgLoginTwoFactorRequiredError, async (e) => {
    const { twoFactorMethod, twoFactorCode } = await inquirer.prompt([
      { 
        type: 'list', 
        name: 'twoFactorMethod', 
        message: '2FA method',
        choices: [
          { name: 'Authenticator', value: '0' },
          { name: 'SMS', value: '1' }
        ]
      },
      { 
        type: 'input', 
        name: 'twoFactorCode', 
        message: '2FA code' 
      }
    ])
    await ig.account.twoFactorLogin({
      twoFactorIdentifier: e.response.body.two_factor_info.two_factor_identifier,
      verificationMethod: twoFactorMethod,
      verificationCode: twoFactorCode,
      username: username
    })
    console.log('two-factor login OK')
  })
  // Challenge
  .catch(IgCheckpointError, async () => {
    console.log('checkpoint', ig.state.checkpoint); // Checkpoint info here
    await ig.challenge.auto(true); // Requesting sms-code or click "It was me" button
    console.log('checkpoint', ig.state.checkpoint); // Challenge info here
    const { code } = await inquirer.prompt([
      { type: 'input', name: 'code', message: 'Challenge code' }
    ])
    console.log(await ig.challenge.sendSecurityCode(code));
    console.log('challenge OK')
  })
}

async function aceptar () {
  console.log('*** check ***')
  await Bluebird.try(async () => {
    const followers = await ig.feed.pendingFriendships().items()
    for (let i in followers) {
      let pending = followers[i]
      if (config.skipSuggestions && !!pending.social_context) {
        continue;
      }
      try {
        console.log(`accept: ${pending.username}`)
        ig.friendship.approve(pending.pk)
      } catch (e) {
        console.error(e)
      }
      await delay(Math.random() * config.interval.requests * 1000)
    }
    console.log(`*** wait ${config.interval.check}s ***`)
  })
  .catch(async (e) => {
    console.error(e)
  })
}

// Main
(async () => {
  await login ();
  await aceptar();
  interval(aceptar, config.interval.check * 1000);
})()
